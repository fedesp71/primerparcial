﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using BE;

namespace Presentacion
{
    public partial class FrmConcepto : Form
    {
        ConceptoBusiness gestor = new ConceptoBusiness();
        public FrmConcepto()
        {
            InitializeComponent();
        }

        private void FrmConcepto_Load(object sender, EventArgs e)
        {
            ListarConceptos();

            TxtDescripcion.Enabled = false;
            NumPorcentaje.Enabled = false;
        }

        public void ListarConceptos()
        {
            DtConceptos.DataSource = null;
            DtConceptos.DataSource = gestor.Listar();
        }

        private void BtnEjecutar_Click(object sender, EventArgs e)
        {
            if (RdAgregar.Checked)
            {
                ConceptoEntity concepto = new ConceptoEntity();
                concepto.Descripcion = TxtDescripcion.Text;
                concepto.PorcentajeAplicado = NumPorcentaje.Value;
                
                gestor.Insertar(concepto);
            }
            if (RdModificar.Checked)
            {
                ConceptoEntity concepto = (ConceptoEntity)DtConceptos.SelectedRows[0].DataBoundItem;

                concepto.Descripcion = TxtDescripcion.Text;
                concepto.PorcentajeAplicado = NumPorcentaje.Value;

                gestor.Editar(concepto);
            }
            if (RdEliminar.Checked)
            {
                ConceptoEntity concepto = (ConceptoEntity)DtConceptos.SelectedRows[0].DataBoundItem;

                gestor.Borrar(concepto);
            }

            ListarConceptos();
        }

        private void DtConceptos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TxtDescripcion.Enabled = true;
            NumPorcentaje.Enabled = true;

            ConceptoEntity concepto = (ConceptoEntity)DtConceptos.SelectedRows[0].DataBoundItem;

            TxtDescripcion.Text = concepto.Descripcion;
            NumPorcentaje.Value = concepto.PorcentajeAplicado;


        }

        private void RdAgregar_CheckedChanged(object sender, EventArgs e)
        {
            TxtDescripcion.Enabled = true;
            NumPorcentaje.Enabled = true;
        }

        private void RdEliminar_CheckedChanged(object sender, EventArgs e)
        {
            TxtDescripcion.Enabled = false;
            NumPorcentaje.Enabled = false;
        }
    }
}
