﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void empleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.MdiParent = this;
            frmEmpleado.Show();
        }

        private void conceptosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConcepto frmConcepto = new FrmConcepto();
            frmConcepto.MdiParent = this;
            frmConcepto.Show();
        }

        private void recibosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmRecibo frmRecibo = new FrmRecibo();
            frmRecibo.MdiParent = this;
            frmRecibo.Show();
        }
    }
}
