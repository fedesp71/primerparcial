﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using BE;

namespace Presentacion
{
    public partial class FrmEmpleado : Form
    {
        EmpleadoBusiness gestor = new EmpleadoBusiness();
        public FrmEmpleado()
        {
            InitializeComponent();
        }

        private void FrmEmpleado_Load(object sender, EventArgs e)
        {
            ListarEmpleados();

            TxtNyApe.Enabled    = false;
            NumCuil.Enabled     = false;
            BtnEjecutar.Enabled = false;
        }

        public void ListarEmpleados()
        {
            DtEmpleados.DataSource = null;
            DtEmpleados.DataSource = gestor.Listar();
        }

        private void BtnEjecutar_Click(object sender, EventArgs e)
        {


            if (RdAgregar.Checked)
            {
                EmpleadoEntity empleado = new EmpleadoEntity();
                empleado.NyApe = TxtNyApe.Text;
                empleado.Cuil = int.Parse(NumCuil.Value.ToString());
                empleado.FecAlta = DateTime.Today;

                gestor.Insertar(empleado);
            }
            if (RdModificar.Checked)
            {
                EmpleadoEntity empleado = (EmpleadoEntity)DtEmpleados.SelectedRows[0].DataBoundItem;

                empleado.NyApe = TxtNyApe.Text;
                empleado.Cuil = int.Parse(NumCuil.Value.ToString());

                gestor.Editar(empleado);
            }
            if (RdEliminar.Checked)
            {
                EmpleadoEntity empleado = (EmpleadoEntity)DtEmpleados.SelectedRows[0].DataBoundItem;

                gestor.Borrar(empleado);
            }

            ListarEmpleados();
        }

        private void DtEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TxtNyApe.Enabled    = true;
            NumCuil.Enabled     = true;
            BtnEjecutar.Enabled = true;
            EmpleadoEntity empleado = (EmpleadoEntity)DtEmpleados.SelectedRows[0].DataBoundItem;
            TxtNyApe.Text = empleado.NyApe;
            NumCuil.Value = empleado.Cuil;

        }

        private void RdAgregar_CheckedChanged(object sender, EventArgs e)
        {
            TxtNyApe.Enabled = true;
            NumCuil.Enabled = true;
            BtnEjecutar.Enabled = true;
        }

        private void RdEliminar_CheckedChanged(object sender, EventArgs e)
        {
            TxtNyApe.Enabled = false;
            NumCuil.Enabled  = false;
        }
    }
}
