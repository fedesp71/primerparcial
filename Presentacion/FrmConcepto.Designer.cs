﻿namespace Presentacion
{
    partial class FrmConcepto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DtConceptos = new System.Windows.Forms.DataGridView();
            this.RdEliminar = new System.Windows.Forms.RadioButton();
            this.RdModificar = new System.Windows.Forms.RadioButton();
            this.RdAgregar = new System.Windows.Forms.RadioButton();
            this.LblPorcentaje = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NumPorcentaje = new System.Windows.Forms.NumericUpDown();
            this.TxtDescripcion = new System.Windows.Forms.TextBox();
            this.BtnEjecutar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPorcentaje)).BeginInit();
            this.SuspendLayout();
            // 
            // DtConceptos
            // 
            this.DtConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtConceptos.Location = new System.Drawing.Point(145, 12);
            this.DtConceptos.Name = "DtConceptos";
            this.DtConceptos.RowTemplate.Height = 24;
            this.DtConceptos.Size = new System.Drawing.Size(579, 150);
            this.DtConceptos.TabIndex = 0;
            this.DtConceptos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtConceptos_CellClick);
            // 
            // RdEliminar
            // 
            this.RdEliminar.AutoSize = true;
            this.RdEliminar.Location = new System.Drawing.Point(17, 106);
            this.RdEliminar.Name = "RdEliminar";
            this.RdEliminar.Size = new System.Drawing.Size(79, 21);
            this.RdEliminar.TabIndex = 7;
            this.RdEliminar.TabStop = true;
            this.RdEliminar.Text = "Eliminar";
            this.RdEliminar.UseVisualStyleBackColor = true;
            this.RdEliminar.CheckedChanged += new System.EventHandler(this.RdEliminar_CheckedChanged);
            // 
            // RdModificar
            // 
            this.RdModificar.AutoSize = true;
            this.RdModificar.Location = new System.Drawing.Point(16, 60);
            this.RdModificar.Name = "RdModificar";
            this.RdModificar.Size = new System.Drawing.Size(86, 21);
            this.RdModificar.TabIndex = 6;
            this.RdModificar.TabStop = true;
            this.RdModificar.Text = "Modificar";
            this.RdModificar.UseVisualStyleBackColor = true;
            // 
            // RdAgregar
            // 
            this.RdAgregar.AutoSize = true;
            this.RdAgregar.Location = new System.Drawing.Point(16, 12);
            this.RdAgregar.Name = "RdAgregar";
            this.RdAgregar.Size = new System.Drawing.Size(80, 21);
            this.RdAgregar.TabIndex = 5;
            this.RdAgregar.TabStop = true;
            this.RdAgregar.Text = "Agregar";
            this.RdAgregar.UseVisualStyleBackColor = true;
            this.RdAgregar.CheckedChanged += new System.EventHandler(this.RdAgregar_CheckedChanged);
            // 
            // LblPorcentaje
            // 
            this.LblPorcentaje.AutoSize = true;
            this.LblPorcentaje.Location = new System.Drawing.Point(332, 205);
            this.LblPorcentaje.Name = "LblPorcentaje";
            this.LblPorcentaje.Size = new System.Drawing.Size(76, 17);
            this.LblPorcentaje.TabIndex = 13;
            this.LblPorcentaje.Text = "Porcentaje";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-154, 189);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "NyApe";
            // 
            // NumPorcentaje
            // 
            this.NumPorcentaje.Location = new System.Drawing.Point(416, 201);
            this.NumPorcentaje.Name = "NumPorcentaje";
            this.NumPorcentaje.Size = new System.Drawing.Size(145, 22);
            this.NumPorcentaje.TabIndex = 11;
            // 
            // TxtDescripcion
            // 
            this.TxtDescripcion.Location = new System.Drawing.Point(145, 205);
            this.TxtDescripcion.Name = "TxtDescripcion";
            this.TxtDescripcion.Size = new System.Drawing.Size(145, 22);
            this.TxtDescripcion.TabIndex = 10;
            // 
            // BtnEjecutar
            // 
            this.BtnEjecutar.Location = new System.Drawing.Point(615, 182);
            this.BtnEjecutar.Name = "BtnEjecutar";
            this.BtnEjecutar.Size = new System.Drawing.Size(114, 58);
            this.BtnEjecutar.TabIndex = 9;
            this.BtnEjecutar.Text = "Ejecutar";
            this.BtnEjecutar.UseVisualStyleBackColor = true;
            this.BtnEjecutar.Click += new System.EventHandler(this.BtnEjecutar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 17);
            this.label3.TabIndex = 14;
            this.label3.Text = "Descripcion";
            // 
            // FrmConcepto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 252);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LblPorcentaje);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NumPorcentaje);
            this.Controls.Add(this.TxtDescripcion);
            this.Controls.Add(this.BtnEjecutar);
            this.Controls.Add(this.RdEliminar);
            this.Controls.Add(this.RdModificar);
            this.Controls.Add(this.RdAgregar);
            this.Controls.Add(this.DtConceptos);
            this.Name = "FrmConcepto";
            this.Text = "FrmConcepto";
            this.Load += new System.EventHandler(this.FrmConcepto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPorcentaje)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DtConceptos;
        private System.Windows.Forms.RadioButton RdEliminar;
        private System.Windows.Forms.RadioButton RdModificar;
        private System.Windows.Forms.RadioButton RdAgregar;
        private System.Windows.Forms.Label LblPorcentaje;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NumPorcentaje;
        private System.Windows.Forms.TextBox TxtDescripcion;
        private System.Windows.Forms.Button BtnEjecutar;
        private System.Windows.Forms.Label label3;
    }
}