﻿namespace Presentacion
{
    partial class FrmRecibo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DtEmpleados = new System.Windows.Forms.DataGridView();
            this.DtConceptos = new System.Windows.Forms.DataGridView();
            this.NumSueldoBruto = new System.Windows.Forms.NumericUpDown();
            this.DtRecibos = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.DtpFecha = new System.Windows.Forms.DateTimePicker();
            this.RdReciboGeneral = new System.Windows.Forms.RadioButton();
            this.RdReciboIndividual = new System.Windows.Forms.RadioButton();
            this.RdAgregar = new System.Windows.Forms.RadioButton();
            this.BtnEjecutar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtConceptos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumSueldoBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtRecibos)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // DtEmpleados
            // 
            this.DtEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtEmpleados.Location = new System.Drawing.Point(12, 66);
            this.DtEmpleados.Name = "DtEmpleados";
            this.DtEmpleados.RowTemplate.Height = 24;
            this.DtEmpleados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtEmpleados.Size = new System.Drawing.Size(514, 150);
            this.DtEmpleados.TabIndex = 1;
            // 
            // DtConceptos
            // 
            this.DtConceptos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtConceptos.Location = new System.Drawing.Point(559, 66);
            this.DtConceptos.Name = "DtConceptos";
            this.DtConceptos.RowTemplate.Height = 24;
            this.DtConceptos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DtConceptos.Size = new System.Drawing.Size(575, 150);
            this.DtConceptos.TabIndex = 2;
            // 
            // NumSueldoBruto
            // 
            this.NumSueldoBruto.Location = new System.Drawing.Point(520, 445);
            this.NumSueldoBruto.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumSueldoBruto.Name = "NumSueldoBruto";
            this.NumSueldoBruto.Size = new System.Drawing.Size(120, 22);
            this.NumSueldoBruto.TabIndex = 3;
            // 
            // DtRecibos
            // 
            this.DtRecibos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtRecibos.Location = new System.Drawing.Point(12, 248);
            this.DtRecibos.Name = "DtRecibos";
            this.DtRecibos.RowTemplate.Height = 24;
            this.DtRecibos.Size = new System.Drawing.Size(1122, 150);
            this.DtRecibos.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(414, 440);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Sueldo Bruto";
            // 
            // DtpFecha
            // 
            this.DtpFecha.CustomFormat = "MM-yyyy";
            this.DtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DtpFecha.Location = new System.Drawing.Point(792, 442);
            this.DtpFecha.Name = "DtpFecha";
            this.DtpFecha.Size = new System.Drawing.Size(200, 22);
            this.DtpFecha.TabIndex = 6;
            // 
            // RdReciboGeneral
            // 
            this.RdReciboGeneral.AutoSize = true;
            this.RdReciboGeneral.Location = new System.Drawing.Point(12, 438);
            this.RdReciboGeneral.Name = "RdReciboGeneral";
            this.RdReciboGeneral.Size = new System.Drawing.Size(225, 21);
            this.RdReciboGeneral.TabIndex = 7;
            this.RdReciboGeneral.TabStop = true;
            this.RdReciboGeneral.Text = "Listar Recibos de un empleado";
            this.RdReciboGeneral.UseVisualStyleBackColor = true;
            this.RdReciboGeneral.CheckedChanged += new System.EventHandler(this.RdReciboGeneral_CheckedChanged);
            // 
            // RdReciboIndividual
            // 
            this.RdReciboIndividual.AutoSize = true;
            this.RdReciboIndividual.Location = new System.Drawing.Point(12, 474);
            this.RdReciboIndividual.Name = "RdReciboIndividual";
            this.RdReciboIndividual.Size = new System.Drawing.Size(175, 21);
            this.RdReciboIndividual.TabIndex = 8;
            this.RdReciboIndividual.TabStop = true;
            this.RdReciboIndividual.Text = "Listar Recibo Individual";
            this.RdReciboIndividual.UseVisualStyleBackColor = true;
            this.RdReciboIndividual.CheckedChanged += new System.EventHandler(this.RdReciboIndividual_CheckedChanged);
            // 
            // RdAgregar
            // 
            this.RdAgregar.AutoSize = true;
            this.RdAgregar.Location = new System.Drawing.Point(12, 517);
            this.RdAgregar.Name = "RdAgregar";
            this.RdAgregar.Size = new System.Drawing.Size(128, 21);
            this.RdAgregar.TabIndex = 9;
            this.RdAgregar.TabStop = true;
            this.RdAgregar.Text = "Agregar Recibo";
            this.RdAgregar.UseVisualStyleBackColor = true;
            this.RdAgregar.CheckedChanged += new System.EventHandler(this.RdAgregar_CheckedChanged);
            // 
            // BtnEjecutar
            // 
            this.BtnEjecutar.Location = new System.Drawing.Point(1010, 498);
            this.BtnEjecutar.Name = "BtnEjecutar";
            this.BtnEjecutar.Size = new System.Drawing.Size(114, 58);
            this.BtnEjecutar.TabIndex = 10;
            this.BtnEjecutar.Text = "Ejecutar";
            this.BtnEjecutar.UseVisualStyleBackColor = true;
            this.BtnEjecutar.Click += new System.EventHandler(this.BtnEjecutar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(687, 445);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Fecha";
            // 
            // FrmRecibo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1146, 571);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BtnEjecutar);
            this.Controls.Add(this.RdAgregar);
            this.Controls.Add(this.RdReciboIndividual);
            this.Controls.Add(this.RdReciboGeneral);
            this.Controls.Add(this.DtpFecha);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DtRecibos);
            this.Controls.Add(this.NumSueldoBruto);
            this.Controls.Add(this.DtConceptos);
            this.Controls.Add(this.DtEmpleados);
            this.Name = "FrmRecibo";
            this.Text = "FrmRecibo";
            this.Load += new System.EventHandler(this.FrmRecibo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtConceptos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumSueldoBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DtRecibos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.DataGridView DtEmpleados;
        private System.Windows.Forms.DataGridView DtConceptos;
        private System.Windows.Forms.NumericUpDown NumSueldoBruto;
        private System.Windows.Forms.DataGridView DtRecibos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DtpFecha;
        private System.Windows.Forms.RadioButton RdReciboGeneral;
        private System.Windows.Forms.RadioButton RdReciboIndividual;
        private System.Windows.Forms.RadioButton RdAgregar;
        private System.Windows.Forms.Button BtnEjecutar;
        private System.Windows.Forms.Label label2;
    }
}