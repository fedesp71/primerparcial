﻿namespace Presentacion
{
    partial class FrmEmpleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DtEmpleados = new System.Windows.Forms.DataGridView();
            this.BtnEjecutar = new System.Windows.Forms.Button();
            this.RdAgregar = new System.Windows.Forms.RadioButton();
            this.RdModificar = new System.Windows.Forms.RadioButton();
            this.RdEliminar = new System.Windows.Forms.RadioButton();
            this.TxtNyApe = new System.Windows.Forms.TextBox();
            this.NumCuil = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DtEmpleados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuil)).BeginInit();
            this.SuspendLayout();
            // 
            // DtEmpleados
            // 
            this.DtEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtEmpleados.Location = new System.Drawing.Point(104, 12);
            this.DtEmpleados.Name = "DtEmpleados";
            this.DtEmpleados.RowTemplate.Height = 24;
            this.DtEmpleados.Size = new System.Drawing.Size(796, 338);
            this.DtEmpleados.TabIndex = 0;
            this.DtEmpleados.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtEmpleados_CellClick);
            // 
            // BtnEjecutar
            // 
            this.BtnEjecutar.Location = new System.Drawing.Point(786, 380);
            this.BtnEjecutar.Name = "BtnEjecutar";
            this.BtnEjecutar.Size = new System.Drawing.Size(114, 58);
            this.BtnEjecutar.TabIndex = 1;
            this.BtnEjecutar.Text = "Ejecutar";
            this.BtnEjecutar.UseVisualStyleBackColor = true;
            this.BtnEjecutar.Click += new System.EventHandler(this.BtnEjecutar_Click);
            // 
            // RdAgregar
            // 
            this.RdAgregar.AutoSize = true;
            this.RdAgregar.Location = new System.Drawing.Point(12, 23);
            this.RdAgregar.Name = "RdAgregar";
            this.RdAgregar.Size = new System.Drawing.Size(80, 21);
            this.RdAgregar.TabIndex = 2;
            this.RdAgregar.TabStop = true;
            this.RdAgregar.Text = "Agregar";
            this.RdAgregar.UseVisualStyleBackColor = true;
            this.RdAgregar.CheckedChanged += new System.EventHandler(this.RdAgregar_CheckedChanged);
            // 
            // RdModificar
            // 
            this.RdModificar.AutoSize = true;
            this.RdModificar.Location = new System.Drawing.Point(12, 71);
            this.RdModificar.Name = "RdModificar";
            this.RdModificar.Size = new System.Drawing.Size(86, 21);
            this.RdModificar.TabIndex = 3;
            this.RdModificar.TabStop = true;
            this.RdModificar.Text = "Modificar";
            this.RdModificar.UseVisualStyleBackColor = true;
            // 
            // RdEliminar
            // 
            this.RdEliminar.AutoSize = true;
            this.RdEliminar.Location = new System.Drawing.Point(13, 117);
            this.RdEliminar.Name = "RdEliminar";
            this.RdEliminar.Size = new System.Drawing.Size(79, 21);
            this.RdEliminar.TabIndex = 4;
            this.RdEliminar.TabStop = true;
            this.RdEliminar.Text = "Eliminar";
            this.RdEliminar.UseVisualStyleBackColor = true;
            this.RdEliminar.CheckedChanged += new System.EventHandler(this.RdEliminar_CheckedChanged);
            // 
            // TxtNyApe
            // 
            this.TxtNyApe.Location = new System.Drawing.Point(201, 401);
            this.TxtNyApe.Name = "TxtNyApe";
            this.TxtNyApe.Size = new System.Drawing.Size(145, 22);
            this.TxtNyApe.TabIndex = 5;
            // 
            // NumCuil
            // 
            this.NumCuil.Location = new System.Drawing.Point(557, 404);
            this.NumCuil.Name = "NumCuil";
            this.NumCuil.Size = new System.Drawing.Size(145, 22);
            this.NumCuil.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(132, 401);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "NyApe";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(499, 404);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "CUIL";
            // 
            // FrmEmpleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(916, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NumCuil);
            this.Controls.Add(this.TxtNyApe);
            this.Controls.Add(this.RdEliminar);
            this.Controls.Add(this.RdModificar);
            this.Controls.Add(this.RdAgregar);
            this.Controls.Add(this.BtnEjecutar);
            this.Controls.Add(this.DtEmpleados);
            this.Name = "FrmEmpleado";
            this.Text = "FrmEmpleado";
            this.Load += new System.EventHandler(this.FrmEmpleado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtEmpleados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumCuil)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DtEmpleados;
        private System.Windows.Forms.Button BtnEjecutar;
        private System.Windows.Forms.RadioButton RdAgregar;
        private System.Windows.Forms.RadioButton RdModificar;
        private System.Windows.Forms.RadioButton RdEliminar;
        private System.Windows.Forms.TextBox TxtNyApe;
        private System.Windows.Forms.NumericUpDown NumCuil;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}