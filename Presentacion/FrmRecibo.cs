﻿using BLL;
using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class FrmRecibo : Form
    {
        EmpleadoBusiness gestorEmpleado = new EmpleadoBusiness();
        ConceptoBusiness gestorConcepto = new ConceptoBusiness();
        ReciboBusiness   gestorRecibo   = new ReciboBusiness();
        public FrmRecibo()
        {
            InitializeComponent();
        }

        private void FrmRecibo_Load(object sender, EventArgs e)
        {
            ListarEmpleados();
            ListarConceptos();
        }

        public void ListarEmpleados()
        {
            DtEmpleados.DataSource = null;
            DtEmpleados.DataSource = gestorEmpleado.Listar();
        }

        public void ListarConceptos()
        {
            DtConceptos.DataSource = null;
            DtConceptos.DataSource = gestorConcepto.Listar();
        }

        private void RdReciboGeneral_CheckedChanged(object sender, EventArgs e)
        {
            NumSueldoBruto.Enabled = false;
            DtpFecha.Enabled       = false;

        }

        private void RdReciboIndividual_CheckedChanged(object sender, EventArgs e)
        {
            NumSueldoBruto.Enabled = false;
            DtpFecha.Enabled       = true;
        }

        private void RdAgregar_CheckedChanged(object sender, EventArgs e)
        {
            NumSueldoBruto.Enabled = true;
            DtpFecha.Enabled       = true;
        }

        private void BtnEjecutar_Click(object sender, EventArgs e)
        {
            DateTime fechita = new DateTime();
            //DtpFecha.Format = "MM-yyyy";
            //DtpFecha.CustomFormat = "MM-yyyy";
            //fechita.Month = DtpFecha.Month;
            fechita.AddMonths = DtpFecha.Month;
            EmpleadoEntity empleado = (EmpleadoEntity)DtEmpleados.SelectedRows[0].DataBoundItem;
            if (RdReciboGeneral.Checked)
            {
                DtRecibos.DataSource = null;
                DtRecibos.DataSource = gestorRecibo.ListarRecibos(empleado);

            }

            if(RdReciboIndividual.Checked)
            {         
                DtRecibos.DataSource = null;
                DtRecibos.DataSource = gestorRecibo.ListarReciboIndivudual(empleado, DtpFecha.Value);

            }

            if(RdAgregar.Checked)
            {
                gestorRecibo.Insertar(ConceptosSeleccionados(), empleado, NumSueldoBruto.Value, DtpFecha.Value);
            }
        }

        public List<ConceptoEntity> ConceptosSeleccionados()
        {
            int FilasSeleccionadas = DtConceptos.Rows.GetRowCount(DataGridViewElementStates.Selected);
            List<ConceptoEntity> conceptos = new List<ConceptoEntity>();
            for (int i = 0; i < FilasSeleccionadas; i++)
            {
                conceptos.Add((ConceptoEntity)DtConceptos.SelectedRows[i].DataBoundItem);
            }

            return conceptos;
        }
    }
}
