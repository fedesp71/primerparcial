﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class ConceptoEntity
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        private decimal porcentajeAplicado;

        public decimal PorcentajeAplicado
        {
            get { return porcentajeAplicado; }
            set { porcentajeAplicado = value; }
        }

    }
}
