﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class ReciboEntity
    {
        private int nroRecibo;

        public int NroRecibo
        {
            get { return nroRecibo; }
            set { nroRecibo = value; }
        }

        private DateTime fecha;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }

        private decimal sueldoBruto;

        public decimal SueldoBruto
        {
            get { return sueldoBruto; }
            set { sueldoBruto = value; }
        }

        private decimal sueldoNeto;

        public decimal SueldoNeto
        {
            get { return sueldoNeto; }
            set { sueldoNeto = value; }
        }

        private decimal totalDescuentos;

        public decimal TotalDescuentos
        {
            get { return totalDescuentos; }
            set { totalDescuentos = value; }
        }

        private List<ConceptoEntity> conceptos  = new List<ConceptoEntity>();

        public List<ConceptoEntity> Conceptos
        {
            get { return conceptos  = new List<ConceptoEntity>(); }
            set { conceptos  =  value; }
        }

        private EmpleadoEntity empleado;

        public EmpleadoEntity Empleado
        {
            get { return empleado; }
            set { empleado = value; }
        }


    }
}
