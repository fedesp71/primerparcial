﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class EmpleadoEntity
    {
        private int nroLegajo;

        public int NroLegajo
        {
            get { return nroLegajo; }
            set { nroLegajo = value; }
        }

        private string nyApe;

        public string NyApe
        {
            get { return nyApe; }
            set { nyApe = value; }
        }

        private int cuil;

        public int Cuil
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fecAlta;

        public DateTime FecAlta
        {
            get { return fecAlta; }
            set { fecAlta = value; }
        }

    }
}
