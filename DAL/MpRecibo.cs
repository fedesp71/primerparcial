﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MpRecibo
    {
        private Acceso acceso = new Acceso();

        public List<ReciboEntity> Listar(EmpleadoEntity empleado)
        {
            List<ReciboEntity> lista = new List<ReciboEntity>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", empleado.NroLegajo));
            DataTable tabla = acceso.Leer("spListarRecibosEmpleado", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ReciboEntity reciboAux = new ReciboEntity();

                reciboAux.NroRecibo = int.Parse(registro["NroRecibo"].ToString());
                reciboAux.Fecha = DateTime.Parse(registro["Fecha"].ToString());
                reciboAux.SueldoBruto = decimal.Parse(registro["SueldoBruto"].ToString());
                reciboAux.SueldoNeto = decimal.Parse(registro["SueldoNeto"].ToString());
                reciboAux.TotalDescuentos = decimal.Parse(registro["TotalDescuentos"].ToString());
              
                lista.Add(reciboAux);
            }
            return lista;
        }


        public List<ReciboEntity> ListarReciboIndividual(EmpleadoEntity empleado, DateTime fecha)
        {
            List<ReciboEntity> lista = new List<ReciboEntity>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", empleado.NroLegajo));
            parameters.Add(acceso.CrearParametro("@Fecha", fecha));
            DataTable tabla = acceso.Leer("spListarReciboIndividualEmpleado", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ReciboEntity reciboAux = new ReciboEntity();

                reciboAux.NroRecibo = int.Parse(registro["NroRecibo"].ToString());
                reciboAux.Fecha = DateTime.Parse(registro["Fecha"].ToString());
                reciboAux.SueldoBruto = decimal.Parse(registro["SueldoBruto"].ToString());
                reciboAux.SueldoNeto = decimal.Parse(registro["SueldoNeto"].ToString());
                reciboAux.TotalDescuentos = decimal.Parse(registro["TotalDescuentos"].ToString());

                lista.Add(reciboAux);
            }
            return lista;
        }

        public List<ConceptoEntity> ListarConceptosDeRecibo(int idReciboConcepto)
        {
            List<ConceptoEntity> lista = new List<ConceptoEntity>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IdRecibo_Concepto", idReciboConcepto));
            DataTable tabla = acceso.Leer("spListarConceptosRecibo", parameters);
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ConceptoEntity conceptoAux = new ConceptoEntity();

                conceptoAux.Descripcion = registro["Descripcion"].ToString();
                conceptoAux.PorcentajeAplicado = decimal.Parse(registro["SueldoBruto"].ToString());

                lista.Add(conceptoAux);
            }
            return lista;
        }

        public void Insertar(ReciboEntity recibo, int IdConcepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@NroRecibo", recibo.NroRecibo));
            parameters.Add(acceso.CrearParametro("@Fecha", recibo.Fecha));
            parameters.Add(acceso.CrearParametro("@SueldoBruto", recibo.SueldoBruto));
            parameters.Add(acceso.CrearParametro("@SueldoNeto", recibo.SueldoNeto));
            parameters.Add(acceso.CrearParametro("@TotalDescuentos", recibo.TotalDescuentos));
            parameters.Add(acceso.CrearParametro("@CodEmpleado", recibo.Empleado.NroLegajo));
            parameters.Add(acceso.CrearParametro("@IdConcepto", IdConcepto));

            acceso.Escribir("spCargarRecibo", parameters);

            acceso.Cerrar();
        }

        public void InsertarConceptosRecibo(ConceptoEntity concepto, int IdConcepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IdRecibo_Concepto", IdConcepto));
            parameters.Add(acceso.CrearParametro("@IdConcepto", concepto.Id));

            acceso.Escribir("spCargarConceptosRecibo", parameters);

            acceso.Cerrar();
        }

        public int ProxRecibo()
        {
            acceso.Abrir();

            DataTable tabla = acceso.Leer("spObtenerUltimoNroRecibo");
            acceso.Cerrar();
            int proxNroRecibo = 0;
            foreach (DataRow registro in tabla.Rows)
            {
                proxNroRecibo = int.Parse(registro["NroRecibo"].ToString());
            }

            return proxNroRecibo + 1;
        }

        public int ProxIdReciboConcepto()
        {
            acceso.Abrir();

            DataTable tabla = acceso.Leer("spObtenerUltimoIdReciboConcepto");
            acceso.Cerrar();
            int proxIdConcepto = 0;
            foreach (DataRow registro in tabla.Rows)
            {
                proxIdConcepto = int.Parse(registro["IdRecibo_Concepto"].ToString());
            }

            return proxIdConcepto + 1;
        }

    }
}
