﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BE;

namespace DAL
{
    public class MpEmpleado
    {
        private Acceso acceso = new Acceso();

        public  List<EmpleadoEntity> Listar()
        {
            List<EmpleadoEntity> empleados = new List<EmpleadoEntity>(); 

            acceso.Abrir();
            DataTable tabla = acceso.Leer("spListarEmpleados");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                EmpleadoEntity empleado = new EmpleadoEntity();
                empleado.NroLegajo = int.Parse(registro["Legajo"].ToString());
                empleado.NyApe = registro["NyApe"].ToString();
                empleado.Cuil = int.Parse(registro["Cuil"].ToString());
                empleado.FecAlta = DateTime.Parse(registro["FecAlta"].ToString());

                empleados.Add(empleado);
            }

            return empleados;
        }

        public void Insertar(EmpleadoEntity empleado)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", empleado.NroLegajo));
            parameters.Add(acceso.CrearParametro("@NyApe", empleado.NyApe));
            parameters.Add(acceso.CrearParametro("@Cuil", empleado.Cuil));
            parameters.Add(acceso.CrearParametro("@FecAlta", empleado.FecAlta));
            acceso.Escribir("spCargarEmpleado", parameters);

            acceso.Cerrar();
        }

        public void Editar(EmpleadoEntity empleado)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", empleado.NroLegajo));
            parameters.Add(acceso.CrearParametro("@NyApe", empleado.NyApe));
            parameters.Add(acceso.CrearParametro("@Cuil", empleado.Cuil));
            acceso.Escribir("spModificarEmpleado", parameters);

            acceso.Cerrar();
        }

        public void Borrar(EmpleadoEntity empleado)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Legajo", empleado.NroLegajo.ToString()));
            acceso.Escribir("spEliminarEmpleado", parameters);

            acceso.Cerrar();
        }


        public int ProxLegajo()
        {
            acceso.Abrir();

            DataTable tabla = acceso.Leer("spObtenerUltimoLegajo");
            acceso.Cerrar();
            int proxLegajo = 0;
            foreach (DataRow registro in tabla.Rows)
            {
                proxLegajo = int.Parse(registro["Legajo"].ToString());
            }
            
            return proxLegajo + 1;
        }
    }
}
