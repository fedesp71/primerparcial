﻿using BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MpConcepto
    {
        private Acceso acceso = new Acceso();

        public List<ConceptoEntity> Listar()
        {
            List<ConceptoEntity> lista = new List<ConceptoEntity>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("spListarConceptos");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                ConceptoEntity conceptoAux = new ConceptoEntity();
                
                
                conceptoAux.Id                 = int.Parse(registro["IdConcepto"].ToString());
                conceptoAux.Descripcion        = registro["Descripcion"].ToString();
                conceptoAux.PorcentajeAplicado = decimal.Parse(registro["PorcentajeAplicado"].ToString());
                lista.Add(conceptoAux);
            }
            return lista;
        }

        public void Insertar(ConceptoEntity concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IdConcepto", concepto.Id));
            parameters.Add(acceso.CrearParametro("@Descripcion", concepto.Descripcion));
            parameters.Add(acceso.CrearParametro("@PorcentajeAplicado", concepto.PorcentajeAplicado));
            acceso.Escribir("spCargarConcepto", parameters);

            acceso.Cerrar();
        }



        public void Editar(ConceptoEntity concepto)
        {

            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IdConcepto", concepto.Id));
            parameters.Add(acceso.CrearParametro("@Descripcion", concepto.Descripcion));
            parameters.Add(acceso.CrearParametro("@PorcentajeAplicado", concepto.PorcentajeAplicado));
            acceso.Escribir("spModificarConcepto", parameters);

            acceso.Cerrar();
        }

        public void Borrar(ConceptoEntity concepto)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@IdConcepto", concepto.Id));
            acceso.Escribir("spEliminarConcepto", parameters);

            acceso.Cerrar();
        }

        public int ProxIdConcepto()
        {
            acceso.Abrir();

            DataTable tabla = acceso.Leer("spObtenerUltimoIdConcepto");
            acceso.Cerrar();
            int proxLegajo = 0;
            foreach (DataRow registro in tabla.Rows)
            {
                proxLegajo = int.Parse(registro["IdConcepto"].ToString());
            }

            return proxLegajo + 1;
        }
    }
}
