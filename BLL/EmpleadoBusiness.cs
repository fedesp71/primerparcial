﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class EmpleadoBusiness
    {
        MpEmpleado mp = new MpEmpleado();
        public List<EmpleadoEntity> Listar()
        {
            return mp.Listar();
        }

        public void Insertar(EmpleadoEntity empleado)
        {
            empleado.NroLegajo = mp.ProxLegajo();
            mp.Insertar(empleado);
        }

        public void Editar(EmpleadoEntity empleado)
        {
            mp.Editar(empleado);
        }

        public void Borrar(EmpleadoEntity empleado)
        {
            mp.Borrar(empleado);
        }


    }
}
