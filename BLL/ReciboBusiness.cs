﻿using BE;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ReciboBusiness
    {
        MpRecibo mp = new MpRecibo();

        public void Insertar(List<ConceptoEntity> conceptos, EmpleadoEntity empleado, decimal sueldoBruto, DateTime fec)
        {
            ReciboEntity recibo = new ReciboEntity();

            recibo.NroRecibo   = mp.ProxRecibo();
            recibo.Fecha       = fec;
            recibo.SueldoBruto = sueldoBruto;
            recibo.Empleado    = empleado;

            int proxIdReciboConceptos = mp.ProxIdReciboConcepto();

            foreach (ConceptoEntity conceptoAux in conceptos)
            {
                recibo.TotalDescuentos += conceptoAux.PorcentajeAplicado;
                mp.InsertarConceptosRecibo(conceptoAux, proxIdReciboConceptos);
            }

            recibo.SueldoNeto = recibo.SueldoBruto - (recibo.SueldoBruto * recibo.TotalDescuentos / 100);

            mp.Insertar(recibo, proxIdReciboConceptos);
        }


        public List<ReciboEntity> ListarRecibos(EmpleadoEntity empleado)
        {
            return mp.Listar(empleado);
        }

        public List<ReciboEntity> ListarReciboIndivudual(EmpleadoEntity empleado, DateTime fecha)
        {
            return mp.ListarReciboIndividual(empleado, fecha);
        }


    }
}
