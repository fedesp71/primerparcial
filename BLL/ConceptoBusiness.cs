﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;
namespace BLL
{
    public class ConceptoBusiness
    {
        MpConcepto mp = new MpConcepto();
        public List<ConceptoEntity> Listar()
        {
            return mp.Listar();
        }

        public void Insertar(ConceptoEntity concepto)
        {
            concepto.Id = mp.ProxIdConcepto();
            mp.Insertar(concepto);
        }

        public void Editar(ConceptoEntity concepto)
        {
            mp.Editar(concepto);
        }

        public void Borrar(ConceptoEntity concepto)
        {
            mp.Borrar(concepto);
        }

    }
}
